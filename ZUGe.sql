USE master;
DROP DATABASE ZuegeDB;
CREATE DATABASE ZuegeDB;
USE ZuegeDB;

CREATE TABLE Bahnhoefe (
	Name			varchar(31)		NOT NULL						PRIMARY KEY,
	Gleise			int,
	LiegtInOrt		varchar(31),
	LiegtInKanton	varchar(31)
);

CREATE TABLE Zuege (
	ZugNr			smallint	PRIMARY KEY,
	Start			varchar(31),
	Ziel			varchar(31)
);

CREATE TABLE Staedte (
	Name			varchar(31),
	Kanton			varchar(31)
);

CREATE TABLE Verbindet (
	Zug				smallint		FOREIGN KEY REFERENCES Zuege(ZugNr),
	Von				varchar(31)		FOREIGN KEY REFERENCES Bahnhoefe(Name),
	Nach			varchar(31),
	Ankunft			varchar(5),
	Abfahrt			varchar(5)
);




/* INSERT Bahnhoefe */ 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Luzern', 15, 'Luzern','Luzern');
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Basel SBB', 17, 'Basel','Basel Stadt'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Basel Bad', 4, 'Basel','Basel Stadt'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Thalwil', 5, 'Thalwil','Z�rich'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Olten', 18, 'Olten','Solothurn'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Horw', 3, 'Horw','Luzern');
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Zug', 3, 'Zug','Zug'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Baden', 3, 'Baden','Aargau'); 
insert into Bahnhoefe (Name, Gleise, LiegtInOrt, LiegtInKanton) 
	values ('Z�rich HB', 3, 'Z�rich','Z�rich');  
/* INSERT Zuege */ 
insert into Zuege (ZugNr, Start, Ziel) 
	values(1, 'Luzern', 'Z�rich HB');
insert into Zuege (ZugNr, Start, Ziel) 
	values(2, 'Z�rich HB', 'Luzern');
insert into Zuege (ZugNr, Start, Ziel) 
	values(3, 'Luzern', 'Horw');
insert into Zuege (ZugNr, Start, Ziel) 
	values(4, 'Horw', 'Luzern');
insert into Zuege (ZugNr, Start, Ziel) 
	values(5, 'Luzern', 'Basel SBB');
insert into Zuege (ZugNr, Start, Ziel) 
	values(6, 'Basel SBB', 'Luzern');
insert into Zuege (ZugNr, Start, Ziel) 
	values(7, 'Z�rich HB', 'Basel Bad');
insert into Zuege (ZugNr, Start, Ziel) 
	values(8, 'Basel Bad', 'Z�rich HB');
/* INSERT Staedte */ 
insert into Staedte (Name, Kanton) 
	values('Luzern', 'Luzern');	
insert into Staedte (Name, Kanton) 
	values('Horw', 'Luzern');	
insert into Staedte (Name, Kanton) 
	values('Zug', 'Zug');	
insert into Staedte (Name, Kanton) 
	values('Z�rich', 'Z�rich');	
insert into Staedte (Name, Kanton) 
	values('Basel', 'Basel Stadt');	
insert into Staedte (Name, Kanton) 
	values('Olten', 'Solothurn');	
insert into Staedte (Name, Kanton) 
	values('Baden', 'Aargau');
insert into Staedte (Name, Kanton) 
	values('Thalwil', 'Z�rich');
/* INSERT Verbindet */ 
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(1, 'Luzern', 'Zug', '12:35', '12:12');	
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(1, 'Zug', 'Thalwil', '12:50', '12:37');	
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(1, 'Thalwil', 'Z�rich HB', '13:05', '12:52');	
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(2, 'Z�rich HB', 'Thalwil', '13:22', '13:15');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(2, 'Thalwil', 'Zug', '13:40', '13:23');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(2, 'Zug', 'Luzern', '14:00', '13:42');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(3, 'Luzern', 'Horw', '14:14', '14:10');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(4, 'Horw', 'Luzern', '14:25', '14:21');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(5, 'Luzern', 'Olten', '14:42', '14:12');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(5, 'Olten', 'Basel SBB', '15:08', '14:45');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(6, 'Basel SBB', 'Olten', '15:33', '15:12');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(6, 'Olten', 'Luzern', '16:05', '15:36');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(7, 'Z�rich HB', 'Baden', '13:37', '13:25');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(7, 'Baden', 'Basel SBB', '14:05', '13:39');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(7, 'Basel SBB', 'Basel Bad', '14:12', '14:08');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(8, 'Basel Bad', 'Basel SBB', '14:30', '14:26');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(8, 'Basel SBB', 'Baden', '15:05', '14:32');
insert into Verbindet (Zug, Von, Nach, Ankunft, Abfahrt) 
	values(8, 'Baden', 'Z�rich HB', '15:20', '15:07');


SELECT * FROM Bahnhoefe;
SELECT * FROM Zuege;
SELECT * FROM Staedte;
SELECT * FROM Verbindet;