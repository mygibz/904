USE [ProduktionsDb]
GO
	/****** Object:  StoredProcedure [dbo].[sp_BestellungHinzufuegen]    Script Date: 15.01.2020 09:31:36 ******/
SET
	ANSI_NULLS ON
GO
SET
	QUOTED_IDENTIFIER ON
GO
	-- =====================================================================================
	-- Author:		Urs Nussbaumer 
	-- Create date: March 2015 
	-- Description:	This stored procedure will add a new order to the table 'Bestellung' and   
	--				it will check, whether this order can be fulfilled or not. If it can be 
	--				fulfilled, a 'Produktionsauftrag' will be created and the status of the 
	--				order will become 2 ('geplant'). Otherwise the status of the order becomes
	--				3 ('nicht erfüllbar' and no record will be inserted into 'Produktionsauftrag'. 
	-- =====================================================================================
	ALTER PROCEDURE [dbo].[sp_BestellungHinzufuegen] @KundeID as int,
	@TeilID as int,
	@Anzahl as int,
	@Liefertermin as smalldatetime AS BEGIN -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET
	NOCOUNT ON;

/*  Mögliche Bestelltstatus:
 1 = nicht definiert
 2 = geplant
 3 = nicht erfüllbar
 4 = abgeschlossen 		*/
declare @Status as tinyint;

set
	@Status = 1;

declare @BestellID as int;

--IF (SELECT Lagerbestand FROM Teil WHERE TeilID=@TeilID) < @Anzahl

-- Bestellung einfügen
INSERT INTO
	Bestellung (
		BestellID,
		KundeID,
		TeilID,
		Anzahl,
		BestellDatum,
		LieferterminWunsch,
		[Status]
	)
VALUES
	(
		IIF(
			(
				SELECT
					COUNT(*)
				FROM
					Bestellung
			) > 0,
			(
				SELECT
					TOP 1 BestellID + 1
				FROM
					Bestellung
				ORDER BY
					BestellID DESC
			),
			1
		),
		@KundeID,
		@TeilID,
		@Anzahl,
		GETDATE(),
		@Liefertermin,
		@Status
	);

INSERT INTO
	Produktionsauftrag (
		ProduktionsauftragID,
		BestellID,
		MaschineBedienungID,
		TeilID,
		Anzahl,
		Produktionstag
	)
VALUES
	(
		IIF(
			(
				SELECT
					COUNT(*)
				FROM
					Produktionsauftrag
			) > 0,
			(
				SELECT
					TOP 1 ProduktionsauftragID + 1
				FROM
					Produktionsauftrag
				ORDER BY
					ProduktionsauftragID DESC
			),
			1
		),
		(
			SELECT
				TOP 1 BestellID
			FROM
				Bestellung
			ORDER BY
				BestellID DESC
		),
		(
			SELECT
				TOP 1 MaschineBedienungID
			FROM
				MaschineBedienung
			WHERE
				MaschineID = (
					SELECT
						TOP 1 MaschineID
					FROM
						Maschine
					WHERE
						MaschineID = (
							SELECT
								TOP 1 MaschineID
							FROM
								MaschineTeil
							WHERE
								TeilID = @TeilID
						)
				)
		),
		@TeilID,
		@Anzahl,
		DATEADD(day, 1, GETDATE())
	);

-- Prüfen, ob die Bestellung erfüllt werden kann (sind die Maschinen 
-- und die Mitarbeiter, welche die Maschinene bedienen verfügbar?)
-- Bestellstatus anpassen und zurückgeben als ReturnValue
SET
	@Status = IIF(
		@Anzahl > DATEDIFF(day, GETDATE(), @Liefertermin),
		IIF(
			DATEADD(
				day,
				IIF(
					(
						SELECT
							Wartungsintervall
						FROM
							Maschine
						WHERE
							MaschineID = (
								SELECT
									TOP 1 MaschineID
								FROM
									MaschineTeil
								WHERE
									TeilID = @TeilID
							)
					) - (
						SELECT
							DATEDIFF(
								day,
								GETDATE(),
								(
									SELECT
										ZuletztGewartet
									FROM
										MaschineWartung
									WHERE
										MaschineID = (
											SELECT
												TOP 1 MaschineID
											FROM
												MaschineTeil
											WHERE
												TeilID = @TeilID
										)
								)
							)
					) > 0,
					0,
					1
				),
				(
					SELECT
						TOP 1 Produktionstag
					FROM
						Produktionsauftrag
					ORDER BY
						ProduktionsauftragID DESC
				)
			) >= GETDATE(),
			2,
			4
		),
		4
	);

UPDATE
	Teil
SET
	Lagerbestand = Lagerbestand - @Anzahl
WHERE
	TeilID = @TeilID;

-- Rückgabe des Bestellstatus 
return @Status;

END