USE master;
DROP DATABASE IF EXISTS qlektro;
CREATE DATABASE qlektro;
USE qlektro;

/*Created Tables*/
CREATE TABLE STATUSZUORDNUNG (
	StatusID			tinyint NOT NULL IDENTITY(1,1) PRIMARY KEY,
	StatusName			varchar(11) 
);
DELETE FROM STATUSZUORDNUNG;
DBCC CHECKIDENT ('STATUSZUORDNUNG', RESEED, 0); /* Reset StatusID https://stackoverflow.com/questions/21824478/reset-identity-seed-after-deleting-records-in-sql-server */

CREATE TABLE MITARBEITER (
	MitarbeiterID		int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Vorname				varchar(48),
	Nachname			varchar(48),
	Rolle				varchar(10),
	StatusID			tinyint FOREIGN KEY REFERENCES STATUSZUORDNUNG(StatusID)
);

CREATE TABLE ZAHLUNGSART (
	ZahlungsartID		int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	ZahlungsartName		varchar(48),
	Beschreibung		varchar(128),
	Preiszuschlag		float
);

CREATE TABLE KATEGORIE (
	KategorieID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	KategorienName		varchar(48),
	Beschreibung		varchar(128)
);

CREATE TABLE LIEFERANT (
	LieferantID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	LieferantName		varchar(48)
);

CREATE TABLE PRODUKT (
	ProduktID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	KategorieID			int FOREIGN KEY REFERENCES KATEGORIE(KategorieID),
	LieferantID			int FOREIGN KEY REFERENCES LIEFERANT(LieferantID),
	ProduktName			varchar(64),
	Beschreibung		varchar(256),
	Preis				float
);


CREATE TABLE RABATT (
	RabattID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Bezeichnung			varchar(48),
	Preisnachlass		float,
	DatumVon			DateTime,
	DatumBis			DateTime
);

CREATE TABLE ARTIKEL (
	ArtikelID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	ProduktID			int FOREIGN KEY REFERENCES PRODUKT(ProduktID),
	RabattID			int FOREIGN KEY REFERENCES RABATT(RabattID),
	Anzahl				int,
	Zwischentotal		float
);

CREATE TABLE LIEFERUNGSART (
	LieferungsartID		int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	LieferungsartName	varchar(48),
	preis				float
);

CREATE TABLE KANTON (
	Kantonskuerzel		char(2) PRIMARY KEY,
	KantonsName			varchar(25)
);

CREATE TABLE PLZ (
	PlzID				int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	PLZ					int,
	Ort					varchar(64),
	Kantonskuerzel		char(2) FOREIGN KEY REFERENCES KANTON(Kantonskuerzel)
);

CREATE TABLE ADRESSE (
	AdressenID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Strasse				varchar(96),
	Hausnummer			varchar(5),
	PlzID				int FOREIGN KEY REFERENCES PLZ(PlzID)
);

CREATE TABLE NUTZER (
	NutzerID			int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AdressenID			int FOREIGN KEY REFERENCES ADRESSE(AdressenID),
	Vorname				varchar(48),
	Nachname			varchar(48),
	Email				varchar(300),
	Passwort			char(64)
);

CREATE TABLE BESTELLUNG (
	BestellungID		int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	NutzerID			int FOREIGN KEY REFERENCES NUTZER(NutzerID),
	MitarbeiterID		int FOREIGN KEY REFERENCES MITARBEITER(MitarbeiterID),
	ZahlungsartID		int FOREIGN KEY REFERENCES ZAHLUNGSART(ZahlungsartID),
	LieferungsartID		int FOREIGN KEY REFERENCES LIEFERUNGSART(LieferungsartID),
	Gesamtpreis			float,
	Datum				DateTime
);


CREATE TABLE BESTELLUNGSARTIKELZUORDNUNG (
	ArtikelID			int FOREIGN KEY REFERENCES ARTIKEL(ArtikelID),
	BestellungID		int FOREIGN KEY REFERENCES BESTELLUNG(BestellungID),
	RabattID			int FOREIGN KEY REFERENCES RABATT(RabattID),
	Gesamtpreis			float
);


/* Insert Data */
INSERT INTO STATUSZUORDNUNG (StatusName)								VALUES ('Unknown'), ('Available'), ('Unavailable'), ('Missing');

/* Insert Mitarbeiter */
INSERT INTO MITARBEITER (Vorname, Nachname, Rolle, StatusID)			VALUES ('Tyler', 'Lee', 'Assistant', 2), ('Bailey', 'Carr', 'Waiter', 3), ('Oliver', 'Meritt', 'Soldier', 2);
UPDATE MITARBEITER SET Vorname='Kai' WHERE MitarbeiterID=1;
DELETE FROM MITARBEITER WHERE MitarbeiterID=3;
INSERT INTO MITARBEITER (Vorname, Nachname, Rolle, StatusID)			VALUES ('Yanik', 'Ammann', 'CEO', 1), ('Samuel', 'Gartmann', 'Questioner' ,1), ('Mauro','Mori', 'Cleaner' ,1), ('Philipp', 'W�rsch', 'Designer', 1);

/* Insert Zahlungsart */
INSERT INTO ZAHLUNGSART (ZahlungsartName, Beschreibung, Preiszuschlag)	VALUES ('Kreditkarte', 'Bezahlen Sie mit Ihrer Kreditkarte', 2.5), ('Bitcoin', 'Bezahlen Sie Anonym und sicher mit Bitcoin', 0.0), 
												('Twint', 'Bezahlen Sie mit Twint', 10.0), ('PayPal', 'Bezahlen Sie mit PayPal, einem einfachen und sicheren Zahlunssystem aus den USA', 4.0);

/* Insert Kanton */
INSERT INTO KANTON (Kantonskuerzel, KantonsName)						VALUES ('ZG', 'Zug'), ('ZH', 'Z�rich'), ('BE', 'Bern'), ('LU', 'Luzern'), ('UR', 'Uri'), ('SZ', 'Schwyz'), ('OW', 'Obwalden'), ('NW', 'Nidwalden'), ('GL', 'Glarus'), 
('FR', 'Freiburg'), ('SO', 'Solothurn'), ('BS', 'Basel-Stadt'), ('BL', 'Basel-Land'), ('SH', 'Schaffhausen'), ('AR', 'Appenzell Ausserrhoden'), ('AI ', 'Appenzell Innerrhoden'), ('SG', 'St. Gallen'), ('GR', 'Graub�nden'), 
('AG', 'Aargau'), ('TG', 'Thurgau'), ('TI', 'Tessin'), ('VD', 'Waadt'), ('VS', 'Wallis'), ('NE', 'Neuenburg'), ('GE', 'Genf'), ('JU', 'Jura');
INSERT INTO PLZ (PLZ, Ort, Kantonskuerzel)								VALUES (6314, 'Unter�geri', 'ZG'), (8091, 'Z�rich', 'ZH'), (9050, 'Appenzell Meistersr�te ', 'AI');
INSERT INTO ADRESSE (Strasse, Hausnummer, PlzID)						VALUES ('H�henweg', 59, 1), ('Niemandsstrasse', 35, 2), ('Nirgendwo', 12, 3);

/* Insert Nutzer */
INSERT INTO NUTZER (AdressenID, Vorname, Nachname, Email, Passwort)		VALUES	(1, 'Kai', 'Steinke', 'contact@babakai.ch', '8648F72A5D0D05FEA3089F5A449E0438'), /* DuckCompany2018! */
																				(1, 'Foo', 'Bar', 'noone@nowhere.lan', '8487E982F5C4A1FFD8B3BC63F4A6FFA1'), /* samuelsamuel */
																				(3, 'Samuel', 'Gartmann', 'kontakt@saaagagamez.ch', '8487E982F5C4A1FFD8B3BC63F4A6FFA1'); /* samuelsamuel */
/* Insert Kategorie */
INSERT INTO KATEGORIE (KategorienName, Beschreibung)					VALUES ('Hardware', 'Elektroteile bestehend aus Transistoren und Silizium'), ('Software', 'Programme und andere Softwarepakete f�r den t�gichen gebrauch');

/* Insert Lieferant */
INSERT INTO LIEFERANT (LieferantName)									VALUES ('Qlektro'), ('Illuminaty'), ('Post'), ('Cicada3301'), ('Apple'), ('Deutsche Bahn'), ('Canonical'), ('Steinke Shokolade');

/* Insert Lieferungsart */
INSERT INTO LIEFERUNGSART (LieferungsartName)							VALUES ('Paket (A-Post)'), ('Paket (B-Post)'), ('Snail Mail');

/* Insert Produkt */ /*(SELECT KategorieID FROM KATEGORIE WHERE KategorienName = 'Elektronik')*/
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 2, 'Revolutionary CPU (DTS317d)', 'This CPU is based on prototypes developed during the third world war. It consists of the best silikon on the market and is capable of everything your digital mind requires.', 600.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 4, 'Inteligent RAM (256GB, 3200MHz)', 'This RAM was designed to solve the constant shortage of not enough space in RAM.', 300.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 6, 'Limited Edition Nvidia 2180-Ti', 'After many requests, we finally started to make our own GPUs. They are compatible with NVIDIAs lineup, but significantly faster and more efficient.', 550.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 3, 'Y420 m8', 'The Y420 m8 is the most recent Product in our lineup of singleboard computers. Its incredibly powerful, even ignoring its size.', 750.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 1, '16QBit Workstation', 'This workstation computer is the first quantum computer available for purchase that actually fits on your desk. Its quite simple and elegant in desing and.. of course.. it has RGB.', 2048.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (1, 2, '2160p Monitor', 'This Monitor uses a highly advanced Liquid-OLED panel (aka: a lot of tiny, glowing fish).', 1052.0);

INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (2, 8, 'Manajro', 'It is based on arch, which allows you to always use the newest Qlektro software available.', 5.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (2, 6, 'MacOS', 'MacOS is known for its unique design (we dont say that its good but its unique). Developed by Apple (not eatable).', 50.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (2, 5, 'Fedora', 'Fedora isnt developed by us, we still think its a great project though and regurarly contribute to it. Fedora is focused on innovation (just like us).', 5.0);
INSERT INTO PRODUKT (KategorieID, LieferantID, ProduktName, Beschreibung, Preis)	VALUES (2, 7, 'Ubuntu', 'Linux for human beings.', 5.0);

/* Insert Rabatt */
INSERT INTO RABATT (Bezeichnung, Preisnachlass)										VALUES ('Kein Rabatt', 0);
INSERT INTO RABATT (Bezeichnung, Preisnachlass, DatumVon, DatumBis)					VALUES ('Black Friday', 50, '2019.11.22', '2019.11.30'), ('Christmas', 42, '2019.12.24', '2019.12.26');


/* Insert Bestellung */
INSERT INTO ARTIKEL (ProduktID, Anzahl)										VALUES (4, 2), (5, 2);
INSERT INTO BESTELLUNG (NutzerID, MitarbeiterID, ZahlungsartID, LieferungsartID, Datum)	VALUES (2, 2, 2, 2, '2019.12.24');
INSERT INTO BESTELLUNGSARTIKELZUORDNUNG (ArtikelID, BestellungID, RabattID) VALUES (1, 1, 2), (2, 1, 2);

INSERT INTO ARTIKEL (ProduktID, Anzahl)										VALUES (1, 3), (7, 1), (9, 1), (10, 2);
INSERT INTO BESTELLUNG (NutzerID, MitarbeiterID, ZahlungsartID, LieferungsartID, Datum) VALUES (1, 1, 1, 1, '2019.03.02');
INSERT INTO BESTELLUNGSARTIKELZUORDNUNG (ArtikelID, BestellungID, RabattID) VALUES (3, 2, 1), (4, 2, 1), (5, 2, 1), (6, 2, 1);

INSERT INTO ARTIKEL (ProduktID, Anzahl)										VALUES (7, 20);
INSERT INTO BESTELLUNG (NutzerID, MitarbeiterID, ZahlungsartID, LieferungsartID, Datum) VALUES (3, 4, 3, 1, '2019.03.02');
INSERT INTO BESTELLUNGSARTIKELZUORDNUNG (ArtikelID, BestellungID, RabattID) VALUES (7, 3, 3);


/* Calculate Preis of Bestellung */
UPDATE ARTIKEL SET Zwischentotal=(ARTIKEL.Anzahl * PRODUKT.Preis - RABATT.Preisnachlass)	
FROM BESTELLUNGSARTIKELZUORDNUNG 
LEFT JOIN ARTIKEL						ON BESTELLUNGSARTIKELZUORDNUNG.ArtikelID = ARTIKEL.ArtikelID
LEFT JOIN RABATT						ON BESTELLUNGSARTIKELZUORDNUNG.RabattID = RABATT.RabattID			
LEFT JOIN PRODUKT						ON ARTIKEL.ProduktID = PRODUKT.ProduktID;

UPDATE BESTELLUNGSARTIKELZUORDNUNG SET Gesamtpreis=ARTIKEL.Zwischentotal	+ ZAHLUNGSART.Preiszuschlag	FROM BESTELLUNG
INNER JOIN ZAHLUNGSART					ON BESTELLUNG.ZahlungsartID = ZAHLUNGSART.ZahlungsartID
INNER JOIN BESTELLUNGSARTIKELZUORDNUNG	ON BESTELLUNGSARTIKELZUORDNUNG.BestellungID = BESTELLUNG.BestellungID
INNER JOIN ARTIKEL						ON BESTELLUNGSARTIKELZUORDNUNG.ArtikelID = ARTIKEL.ArtikelID;


SELECT Vorname, Nachname, Rolle, STATUSZUORDNUNG.StatusName FROM MITARBEITER
INNER JOIN STATUSZUORDNUNG				ON MITARBEITER.StatusID = STATUSZUORDNUNG.StatusID;

/*SELECT ZahlungsartName, Beschreibung, Preiszuschlag FROM ZAHLUNGSART;*/

SELECT PRODUKT.ProduktName	AS Produkt, PRODUKT.Beschreibung, PRODUKT.Preis, KATEGORIE.KategorienName AS Kategorie, LIEFERANT.LieferantName AS Lieferant FROM PRODUKT 
LEFT JOIN KATEGORIE			ON PRODUKT.KategorieID = KATEGORIE.KategorieID 
LEFT JOIN LIEFERANT			ON LIEFERANT.LieferantID = PRODUKT.LieferantID;

/*SELECT (Strasse + Hausnummer) AS Strasse, PLZ.Ort, PLZ.PLZ, KANTON.KantonsName AS Kanton FROM ADRESSE LEFT JOIN PLZ ON ADRESSE.PlzID = PLZ.PlzID LEFT JOIN KANTON ON PLZ.Kantonskuerzel = KANTON.Kantonskuerzel;*/

SELECT Vorname, Nachname, Email, (Strasse +  ' ' + Hausnummer) AS Strasse, PLZ.Ort, PLZ.PLZ, KANTON.KantonsName AS Kanton FROM NUTZER LEFT JOIN ADRESSE ON ADRESSE.AdressenID = NUTZER.AdressenID
LEFT JOIN PLZ ON ADRESSE.PlzID = PLZ.PlzID LEFT JOIN KANTON ON PLZ.Kantonskuerzel = KANTON.Kantonskuerzel;

/*SELECT PRODUKT.ProduktName AS Produkt, Anzahl, RABATT.Bezeichnung, Zwischentotal FROM ARTIKEL 
LEFT JOIN PRODUKT ON ARTIKEL.ProduktID = PRODUKT.ProduktID 
LEFT JOIN RABATT ON ARTIKEL.RabattID = RABATT.RabattID;*/

SELECT BESTELLUNG.BestellungID, 
(NUTZER.Vorname + ' ' + NUTZER.Nachname)				AS Kunde, 
(MITARBEITER.Vorname + ' ' + MITARBEITER.Nachname)		AS Mitarbeiter, 
ZAHLUNGSART.ZahlungsartName								AS Zahlungsart, 
LIEFERUNGSART.LieferungsartName							AS Lieferungsart, 
FORMAT(Datum, 'dd.MM.yyyy')								AS Bestelldatum, /* https://database.guide/how-to-format-the-date-time-in-sql-server/ */
'CHF ' +  LTRIM(Str(PRODUKT.Preis))						AS Einzelpreis, 
LTRIM(Str(ARTIKEL.Anzahl)) + 'x ' +PRODUKT.ProduktName	AS Produkt,
RABATT.Bezeichnung AS Rabattbezeichnung, 'CHF -' +  LTRIM(Str(RABATT.Preisnachlass))	AS Rabattpreisnachlass, 
'CHF ' +  LTRIM(Str(ZAHLUNGSART.Preiszuschlag))											AS Zahlungsartpreiszuschlag, 
'CHF ' +  LTRIM(Str(BESTELLUNGSARTIKELZUORDNUNG.Gesamtpreis))							AS Gesamtpreis, 
ADRESSE.Strasse + ' ' + ADRESSE.Hausnummer + ', ' + LTRIM(Str(PLZ.PLZ)) + ' ' + PLZ.Ort + ' ' + KANTON.Kantonskuerzel AS Adresse
FROM BESTELLUNG LEFT JOIN NUTZER			ON BESTELLUNG.NutzerID = NUTZER.NutzerID 
LEFT JOIN MITARBEITER						ON BESTELLUNG.MitarbeiterID = MITARBEITER.MitarbeiterID 
LEFT JOIN ZAHLUNGSART						ON ZAHLUNGSART.ZahlungsartID = BESTELLUNG.ZahlungsartID 
LEFT JOIN LIEFERUNGSART						ON LIEFERUNGSART.LieferungsartID = BESTELLUNG.LieferungsartID
LEFT JOIN BESTELLUNGSARTIKELZUORDNUNG		ON BESTELLUNGSARTIKELZUORDNUNG.BestellungID = BESTELLUNG.BestellungID
INNER JOIN ARTIKEL							ON BESTELLUNGSARTIKELZUORDNUNG.ArtikelID = ARTIKEL.ArtikelID 
INNER JOIN PRODUKT							ON ARTIKEL.ProduktID = PRODUKT.ProduktID
INNER JOIN RABATT							ON BESTELLUNGSARTIKELZUORDNUNG.RabattID = RABATT.RabattID
LEFT JOIN ADRESSE							ON NUTZER.AdressenID = ADRESSE.AdressenID
LEFT JOIN PLZ								ON PLZ.PlzID = ADRESSE.PlzID
LEFT JOIN KANTON							ON PLZ.Kantonskuerzel = KANTON.Kantonskuerzel
GROUP BY BESTELLUNG.BestellungID, NUTZER.Vorname, NUTZER.Nachname, MITARBEITER.Vorname, MITARBEITER.Nachname, ZAHLUNGSART.ZahlungsartName, LIEFERUNGSART.LieferungsartName, BESTELLUNG.Datum, PRODUKT.ProduktName, 
ARTIKEL.Anzahl, PRODUKT.Preis, RABATT.Bezeichnung, RABATT.Preisnachlass, ZAHLUNGSART.Preiszuschlag, BESTELLUNG.Gesamtpreis, ADRESSE.Strasse, ADRESSE.Hausnummer, PLZ.PLZ, PLZ.Ort, KANTON.KantonsName, 
BESTELLUNGSARTIKELZUORDNUNG.Gesamtpreis, KANTON.Kantonskuerzel;