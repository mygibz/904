DROP LOGIN meier;
DROP LOGIN moser;
DROP USER meier;
DROP USER moser;

CREATE LOGIN meier WITH PASSWORD='pwd';
CREATE LOGIN moser WITH PASSWORD='pwd';

--USE ZuegeDB;
use qlektro;

CREATE USER meier FOR LOGIN meier;
CREATE USER moser FOR LOGIN moser;

GRANT SELECT ON Bahnhoefe TO meier WITH GRANT OPTION;
GRANT SELECT ON Zuege TO meier WITH GRANT OPTION;
GRANT SELECT ON Verbindet TO meier WITH GRANT OPTION;

--EXEC sp_addrolemember 'db_datareader', 'moser';
--EXEC sp_addrolemember 'db_datawriter', 'moser';
GRANT SELECT, UPDATE, INSERT ON Bahnhoefe TO moser WITH GRANT OPTION;
GRANT SELECT, UPDATE, INSERT ON Zuege TO moser WITH GRANT OPTION;
GRANT SELECT, UPDATE, INSERT ON Verbindet TO moser WITH GRANT OPTION;

REVOKE DELETE ON Bahnhoefe FROM moser;
REVOKE DELETE ON Zuege FROM moser;
REVOKE DELETE ON Staedte FROM moser;
REVOKE DELETE ON Verbindet FROM moser;