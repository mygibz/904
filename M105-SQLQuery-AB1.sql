USE		master;
DROP	DATABASE	UniDB;
CREATE	DATABASE	UniDB;
USE		UniDB;

CREATE TABLE Professoren (
	PersNr			tinyint			NOT NULL		IDENTITY(1,1)	PRIMARY KEY,
	Vorname			varchar(31),
	Nachname		varchar(31),
	Geburtsdatum	date,
	Raum			varchar(5)		NOT NULL		UNIQUE
);

CREATE TABLE Assistenten (
	PersNr			tinyint			NOT NULL		IDENTITY(1,1)	PRIMARY KEY,
	Vorname			varchar(31),
	Nachname		varchar(31),
	ProfessorenID	tinyint			FOREIGN KEY REFERENCES Professoren(PersNr)
);

CREATE TABLE Vorlesungen (
	VorlNr			smallint		NOT NULL		IDENTITY(1,1)	PRIMARY KEY,
	ProfessorenNr	tinyint			FOREIGN KEY REFERENCES Professoren(PersNr),
	Titel			varchar(42)
);

CREATE TABLE Studenten (
	MatrNr			char(9)			NOT NULL						PRIMARY KEY,
	Vorname			varchar(31),
	Nachname		varchar(31),
	Studienbeginn	date,
	Geburtsdatum	datetime
);

CREATE TABLE Besuch (
	VorlNr			smallint		FOREIGN KEY REFERENCES			Vorlesungen(VorlNr),
	MartNR			char(9)			FOREIGN KEY REFERENCES			Studenten(MatrNr)
);

CREATE TABLE Pruefung (
	Note			float,
	ProfessorenNr	tinyint			FOREIGN KEY REFERENCES			Professoren(PersNr),
	VorlNr			smallint		FOREIGN KEY REFERENCES			Vorlesungen(VorlNr),
	MartNr			char(9)			FOREIGN KEY REFERENCES			Studenten(MatrNr),
	Pruefungstermin	datetime
);


ALTER TABLE	Professoren		ALTER COLUMN	Vorname		varchar(31)		NOT NULL;
ALTER TABLE	Professoren		ALTER COLUMN	Nachname	varchar(31)		NOT NULL;

ALTER TABLE	Assistenten		ALTER COLUMN	Vorname		varchar(31)		NOT NULL;
ALTER TABLE	Assistenten		ALTER COLUMN	Nachname	varchar(31)		NOT NULL;

ALTER TABLE	Studenten		ALTER COLUMN	Vorname		varchar(31)		NOT NULL;
ALTER TABLE	Studenten		ALTER COLUMN	Nachname	varchar(31)		NOT NULL;


ALTER TABLE Studenten		DROP COLUMN		Geburtsdatum;
ALTER TABLE Assistenten		ADD				Geburtsdatum			date;


INSERT INTO	Professoren (Nachname, Vorname, Raum, Geburtsdatum) VALUES ('Einstein', 'Albert', 14, '18790314'), ('Curie', 'Marie', 12, '18671107'),
			('Von Matt', 'Peter', 8, '19370520'), ('Tesla', 'Nikola', 13, '18560710'), ('R�ntgen', 'Wilhelm Conrad', 10, '18450327'), 
			('Sartre', 'Jean-paul', 1, '19050321'), ('van''t Hoff', 'Jacobus', 3, '18520803');

INSERT INTO Assistenten (Nachname, Vorname, Geburtsdatum, ProfessorenID) VALUES 
			('Messi', 'Lionel', '19870624', NULL),
			('Shaqiri', 'Xherdan', '19911010', (SELECT PersNr FROM Professoren WHERE Vorname='Nikola' AND Nachname='Tesla')),
			('Khedira', 'Sami', '19870404', (SELECT PersNr FROM Professoren WHERE Vorname='Peter' AND Nachname='Von Matt')),
			('Ronaldo', 'Cristiano', '19850205', (SELECT PersNr FROM Professoren WHERE Vorname='Peter' AND Nachname='Von Matt')),
			('Rooney', 'Wayne', '19851024', NULL),
			('Robben', 'Arjen', '19840123', (SELECT PersNr FROM Professoren WHERE Vorname='Jacobus' AND Nachname='va ''t Hogg')),
			('Pirlo', 'Andrea', '19790519', (SELECT PersNr FROM Professoren WHERE Vorname='Albert' AND Nachname='Einstein')),
			('Lewandowski', 'Robert', '19880821', (SELECT PersNr FROM Professoren WHERE Vorname='peter' AND Nachname='Von Matt')),
			('Su�rez', 'Luis', '19870124', (SELECT PersNr FROM Professoren WHERE Vorname='Wilhelm Conrad' AND Nachname='R�ntgen')),
			('Sanchez', 'Alexis', '19881219', (SELECT PersNr FROM Professoren WHERE Vorname='Marie' AND Nachname='Curie'));

INSERT INTO Studenten (MatrNr, Nachname, Vorname, Studienbeginn) VALUES
			('09-4845-0', 'Burri', 'Eliane', '20091001'),		('12-5776-4', 'Duss', 'Guido', '20121001'),			('08-5694-8', 'Antonelli', 'Giorgio', '20081001'),
			('09-1079-4', 'Di Lavello', 'Paolo', '20091001'),	('10-5068-5', 'Meier', 'Rolf', '20101001'),			('09-9370-0', 'Maggi', 'Marco', '20091001'),
			('11-8456-6', 'Dubach', 'Hans', '20111001'),		('10-3201-8', 'Keller', 'Yvonne', '20101001'),		('13-9475-7', 'Ott', 'Otto', '20131001'),
			('10-0155-1', 'Meier', 'Isabelle', '20101001'),		('13-1882-4', 'Heller', 'Jacqueline', '20131001'),	('12-8495-0', 'Bachmann', 'Heinz', '20121001'),
			('13-3704-2', 'Genkinger', 'Marian', '20131001'),	('12-7484-5', 'Borter', 'Elisabeth', '20121001'),	('07-3035-1', 'N�sberger', 'Nicole', '20071001'),
			('14-9685-8', 'Benz', 'Nicole', '20071001'),		('10-6943-9', 'Kurman', 'Max', '20101001'),			('12-8867-9', 'Dell''Amore', 'Michaele', '20071001');

INSERT INTO Vorlesungen (Titel, ProfessorenNr) VALUES
			('Ethik', (SELECT PersNr FROM Professoren WHERE Vorname='Peter' AND Nachname='Von Matt')),
			('Elektrotechnik', (SELECT PersNr FROM Professoren WHERE Vorname='Nikola' AND Nachname='Tesla')),
			('Sprachtheorie', (SELECT PersNr FROM Professoren WHERE Vorname='Peter' AND Nachname='Von Matt')),
			('Radiologie', (SELECT PersNr FROM Professoren WHERE Vorname='Wilhelm Conrad' AND Nachname='R�ntgen')),
			('Allgemeine Relativit�tstheorie', (SELECT PersNr FROM Professoren WHERE Vorname='Albert' AND Nachname='Einstein')),
			('Quantenphysik', (SELECT PersNr FROM Professoren WHERE Vorname='Albert' AND Nachname='Einstein')),
			('Kinetik', (SELECT PersNr FROM Professoren WHERE Vorname='Jacobus' AND Nachname='van''t Hoff')),
			('Literaturgeschichte', (SELECT PersNr FROM Professoren WHERE Vorname='Peter' AND Nachname='Von Matt')),
			('Astrophysik', (SELECT PersNr FROM Professoren WHERE Vorname='Albert' AND Nachname='Einstein')),
			('Biochemie', (SELECT PersNr FROM Professoren WHERE Vorname='Jacobus' AND Nachname='van''t Hoff')),
			('Physikalische Chemie', (SELECT PersNr FROM Professoren WHERE Vorname='Marie' AND Nachname='Curie'));

INSERT INTO Besuch (MartNR, VorlNr) VALUES
			('12-8867-9', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik')),
			('07-3035-1', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik')),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik')),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Literaturgeschichte')),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Sprachtheorie')),
			('10-0155-1', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Kinetik')),
			('13-9475-7', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Allgemeine Relativit�tstheorie')),
			('13-9475-7', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik')),
			('10-3201-8', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Kinetik')),
			('09-9370-0', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Literaturgeschichte')),
			('12-5776-4', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Quantenphysik')),
			('09-1079-4', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Biochemie'));

INSERT INTO Pruefung (MartNr, VorlNr, ProfessorenNr, Pruefungstermin, Note) VALUES
			('12-8867-9', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik'), (SELECT PersNr FROM Professoren WHERE Nachname='Sartre' AND Vorname='Jean-paul'), '2014-04-13T14:00:00', 4),
			('07-3035-1', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik'), (SELECT PersNr FROM Professoren WHERE Nachname='Sartre' AND Vorname='Jean-paul'), NULL, NULL),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik'), (SELECT PersNr FROM Professoren WHERE Nachname='Von Matt' AND Vorname='Peter'), '2016-05-25T09:30:00', NULL),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Literaturgeschichte'), (SELECT PersNr FROM Professoren WHERE Nachname='Von Matt' AND Vorname='Peter'), '2016-05-27T09:00:00', NULL),
			('13-3704-2', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Sprachtheorie'), (SELECT PersNr FROM Professoren WHERE Nachname='Von Matt' AND Vorname='Peter'), NULL, NULL),
			('10-0155-1', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Kinetik'), (SELECT PersNr FROM Professoren WHERE Nachname='van''t hoff' AND Vorname='Jacobus'), '2014-05-28T10:00:00', 5.5),
			('13-9475-7', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Ethik'), (SELECT PersNr FROM Professoren WHERE Nachname='Sartre' AND Vorname='Jean-Paul'), '2014-02-24T10:00:00', 3.5),
			('10-3201-8', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Kinetik'), (SELECT PersNr FROM Professoren WHERE Nachname='van''t hoff' AND Vorname='Jacobus'), NULL, NULL),
			('09-9370-0', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Literaturgeschichte'), (SELECT PersNr FROM Professoren WHERE Nachname='Von Matt' AND Vorname='Peter'), '2014-03-03T15:30:00', 4),
			('12-5776-4', (SELECT VorlNr FROM Vorlesungen WHERE Titel='Quantenphysik'), (SELECT PersNr FROM Professoren WHERE Nachname='Tesla' AND Vorname='Nikola'), '2014-11-28T14:00:00', 4.5);




CREATE TABLE Ort_tmp (
	ONRP			int			NOT NULL							PRIMARY KEY,
	Typ				tinyint		NOT NULL,
	PLZ				smallint	NOT NULL,
	PLZZusatz		smallint	NOT NULL,
	Ort18			varchar(18)	NOT NULL,
	Ort27 varchar(28)			NOT NULL,
	Kanton char(2)				NOT NULL
);
CREATE TABLE Ort (
	ONRP			int			NOT NULL			IDENTITY(1,1)	PRIMARY KEY,
	PLZ				smallint	NOT NULL, 
	Ortsbezeichnung	varchar(27) NOT NULL,
	Kanton			char(2)		NOT NULL
 ); 

BULK INSERT Ort_tmp
FROM 'C:\Users\M105TLN\Desktop\121_Verzeichnis-PLZ-light.csv'
WITH (
	FIELDTERMINATOR=';',
	ROWTERMINATOR='\n',
	FIRSTROW=2,
	CODEPAGE='RAW'
);

INSERT INTO Ort (PLZ, Ortsbezeichnung, Kanton)		SELECT PLZ, Ort27, Kanton				FROM Ort_tmp WHERE Typ = 10 OR Typ = 20;
DROP TABLE Ort_tmp;

SELECT * FROM Besuch;

UPDATE Professoren SET Raum=2 WHERE Vorname='Marie' AND Nachname='Curie';
UPDATE Vorlesungen SET ProfessorenNr=(SELECT PersNr FROM Professoren WHERE Vorname='Marie' AND Nachname='Curie') WHERE Titel='Biochemie';
UPDATE Pruefung SET Pruefungstermin='2015-12-09' WHERE MartNr='10-3201-8' AND VorlNr=(SELECT VorlNr FROM Vorlesungen WHERE Titel='Kinetik');
UPDATE Studenten SET Vorname='Isabel', Nachname='Meyer' WHERE Vorname='Isabelle' AND Nachname='Meier';
--UPDATE Studenten SET Wohnort=(SELECT ONRP FROM Ort WHERE Ortsbezeichnung='Wil' AND Kantonsbezeichnung='AG') WHERE MatrNr LIKE '%4';

DELETE FROM Assistenten WHERE Vorname='Wayne' AND Nachname='Rooney';
DELETE FROM Studenten WHERE Nachname='Brunner';
DELETE FROM Vorlesungen WHERE VorlNr NOT IN (SELECT VorlNr FROM Besuch WHERE VorlNr IS NOT NULL);
SELECT * FROM Vorlesungen WHERE VorlNr NOT IN (SELECT VorlNr FROM Besuch WHERE VorlNr IS NOT NULL);


SELECT * FROM Ort;
SELECT TABLE_NAME		AS Tabelle															FROM INFORMATION_SCHEMA.TABLES;
SELECT *																					FROM Professoren;
SELECT Assistenten.PersNr, Assistenten.Vorname + ' ' + Assistenten.Nachname AS Name, 
Professoren.Vorname + ' ' + Professoren.Nachname AS Professor, Assistenten.Geburtsdatum		FROM Assistenten
LEFT JOIN Professoren ON Assistenten.ProfessorenID = Professoren.PersNr;
SELECT *																					FROM Vorlesungen;
SELECT *																					FROM Studenten;
SELECT Titel, MartNR AS Studentennummer, Vorname + ' ' + Nachname AS Student				FROM Besuch
LEFT JOIN Vorlesungen ON Besuch.VorlNr = Vorlesungen.VorlNr LEFT JOIN Studenten ON Besuch.MartNR = Studenten.MatrNr;
SELECT *																					FROM Pruefung;


SELECT ' ' AS 'Nothing here';


SELECT Raum FROM Professoren WHERE Vorname='Marie' AND Nachname='Curie';
SELECT MatrNr, Vorname, Nachname FROM Studenten WHERE MatrNr='14-05565-8';
SELECT VorlNr, Titel FROM Vorlesungen ORDER BY Titel ASC;
SELECT ProfessorenNr, Titel FROM Vorlesungen ORDER BY ProfessorenNr ASC, Titel DESC;
SELECT DISTINCT VorlNr FROM Besuch;
SELECT * FROM Studenten WHERE Vorname='Roger';
SELECT MatrNr, Vorname, Nachname FROM Studenten WHERE LEFT(Vorname, 1)='P';
SELECT MatrNr, Vorname, Nachname FROM Studenten WHERE Vorname='Hans' OR Vorname='Anita' OR Vorname='Roger' ORDER BY Vorname ASC;
SElECT Vorname + ' ' + Nachname AS 'Assistenten ohne Chef' FROM Assistenten WHERE ProfessorenID IS NULL;
SELECT Vorname, Nachname FROM Studenten WHERE LEFT(Vorname, 1)='G' AND RIGHT(Vorname, 1)='o';
SELECT Vorname FROM Studenten WHERE Nachname = 'Dell''Amore';
SELECT Vorname, Nachname FROM Studenten WHERE Vorname='Roger' OR Nachname='Meier';
SELECT MatrNr, Vorname, Nachname, Studienbeginn FROM Studenten WHERE Studienbeginn > '20140101';
SELECT COUNT(*) FROM Pruefung WHERE Pruefungstermin IS NULL;
SELECT MartNR, COUNT(MartNR) AS 'Vorlesungen Besucht' FROM Besuch GROUP BY MartNR HAVING COUNT(MartNR) > 2;
SELECT TOP 1 Geburtsdatum FROM Assistenten ORDER BY Geburtsdatum DESC;
SELECT TOP 1 Pruefungstermin FROM Pruefung WHERE Pruefungstermin IS NOT NULL ORDER BY Pruefungstermin ASC;
--AB 18
SELECT Count(*) FROM Studenten WHERE Studienbeginn='20131001';
SELECT TOP 1 Vorname, Nachname, Geburtsdatum FROM Assistenten ORDER BY Geburtsdatum ASC;
SELECT AVG(Note) FROM Pruefung LEFT JOIN Vorlesungen ON Vorlesungen.VorlNr=Vorlesungen.VorlNr WHERE Titel='Literaturgeschichte';
SELECT MartNr, AVG(Note) AS 'Durchschnitsnote' FROM Pruefung GROUP BY MartNr HAVING AVG(Note)>=4.0 AND AVG(Note)<=4.5;


SELECT ' ' AS 'Nothing here';

--1
SELECT VorlNr FROM Besuch WHERE MartNR='14-0556-8';

--AB2

--3
SELECT DISTINCT Studenten.Nachname FROM Besuch INNER JOIN Studenten ON Studenten.MatrNr=Besuch.MartNR;

--4
SELECT DISTINCT Titel, Vorname, Nachname FROM Vorlesungen INNER JOIN Professoren ON Vorlesungen.ProfessorenNr=Professoren.PersNr ORDER BY Titel ASC;

--5
SELECT Pruefungstermin, Note, Vorname, Nachname FROM Studenten INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr WHERE Note BETWEEN 4.0 AND 5.0;

--6
SELECT (Professoren.Vorname + ' ' + Professoren.Nachname) AS Professor, Assistenten.Vorname, Assistenten.Nachname FROM Professoren INNER JOIN Assistenten 
ON Assistenten.ProfessorenID=Professoren.PersNr ORDER BY Professoren.Nachname ASC;

--7
SELECT Professoren.Vorname, Professoren.Nachname, COUNT(*) AS 'Anzahl Assistenten' FROM Professoren INNER JOIN Assistenten ON Assistenten.ProfessorenID=Professoren.PersNr 
GROUP BY Professoren.PersNr, Professoren.Vorname, Professoren.Nachname HAVING COUNT(*) > 1;

--8
SELECT Professoren.Vorname, Professoren.Nachname FROM Professoren LEFT JOIN Assistenten ON Assistenten.ProfessorenID=Professoren.PersNr
GROUP BY Professoren.Vorname, Professoren.Nachname HAVING Count(Assistenten.ProfessorenID) = 0;

--9
SELECT Pruefungstermin, Studenten.MatrNr, Studenten.Vorname, Studenten.Nachname, Vorlesungen.Titel FROM Pruefung
INNER JOIN Studenten ON Pruefung.MartNr=Studenten.MatrNr
INNER JOIN Vorlesungen ON Vorlesungen.VorlNr=Pruefung.VorlNr
WHERE Pruefungstermin BETWEEN '20140101' AND '20141231';

--10
SELECT Studenten.MatrNr, (Studenten.Vorname + ' ' + Studenten.Nachname) AS 'Student', Professoren.Raum, Vorlesungen.Titel, (Professoren.Vorname + ' ' + Professoren.Nachname) AS 'Professor' FROM Studenten
INNER JOIN Besuch ON Besuch.MartNR=Studenten.MatrNr INNER JOIN Vorlesungen ON Vorlesungen.VorlNr=Besuch.VorlNr INNER JOIN Professoren ON Professoren.PersNr=Vorlesungen.ProfessorenNr;

--11
SELECT VorlNr, Titel FROM Vorlesungen WHERE VorlNr < (SELECT VorlNr FROM Vorlesungen WHERE Titel='Quantenphysik');

--12
SELECT Professoren.PersNr, Professoren.Vorname, Professoren.Nachname, Professoren.Raum, Professoren.Geburtsdatum, COUNT(*) AS 'Anzahl Assistenten', Assistenten.Vorname, Assistenten.Nachname FROM Professoren
INNER JOIN Assistenten ON Assistenten.ProfessorenID=Professoren.PersNr 
GROUP BY Professoren.PersNr, Professoren.Vorname, Professoren.Nachname, Professoren.Raum, Professoren.Geburtsdatum, Assistenten.Vorname, Assistenten.Nachname HAVING COUNT(*) > 0;

--13
SELECT Studenten.MatrNr, Studenten.Vorname, Studenten.Nachname, Titel FROM Studenten
LEFT JOIN Besuch ON Besuch.MartNR=Studenten.MatrNr LEFT JOIN Vorlesungen ON Besuch.VorlNr=Vorlesungen.VorlNr;

--14
SELECT Assistenten.Vorname, Assistenten.Nachname FROM Assistenten
INNER JOIN Professoren ON Professoren.PersNr=Assistenten.ProfessorenID WHERE Professoren.Vorname='Peter' AND Professoren.Nachname='Von Matt';

--15
SELECT Vorname, Nachname, Pruefungstermin FROM Studenten
INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr WHERE Pruefungstermin IS NULL;

--16
SELECT TOP 1 Vorname, Nachname, Note FROM Studenten
INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr WHERE Note IS NOT NULL ORDER BY Note DESC;

--17
SELECT Vorname, Nachname, COUNT(PersNr) AS 'Anzahl Vorlesungen' FROM Professoren 
INNER JOIN Vorlesungen ON Vorlesungen.ProfessorenNr=Professoren.PersNr 
GROUP BY Vorname, Nachname HAVING COUNT(PersNr) > 1;

--18
SELECT Titel, Note FROM Pruefung 
INNER JOIN Vorlesungen ON Pruefung.VorlNr=Vorlesungen.VorlNr
GROUP BY Vorlesungen.VorlNr, Note, Titel
HAVING AVG(Note) < 4;

--19
SELECT Vorname, Nachname, 'P' AS Personentyp FROM Professoren
UNION  
SELECT Vorname, Nachname, 'S' AS Personentyp FROM Studenten
UNION  
SELECT Vorname, Nachname, 'A' AS Personentyp FROM Assistenten;

--20
SELECT MatrNr, Vorname, Nachname, AVG(Note) AS 'Notendurchschnitt',
case
when AVG(Note) < 4.0 then 'ungen�gend'
when AVG(Note) BETWEEN 4.0 AND 4.9 then 'gen�gend'
when AVG(Note) BETWEEN 5.0 AND 5.4 then 'gut'
when AVG(Note) BETWEEN 5.5 AND 6.0 then 'sehr gut'
end AS Qualifikation
FROM Studenten 
INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr
GROUP BY MatrNr, Note, Vorname, Nachname HAVING Note IS NOT NULL
ORDER BY AVG(Note) DESC;

--21
SELECT Vorname, Nachname, Pruefungstermin, Note FROM Studenten
INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr 
WHERE Studenten.MatrNr=
(SELECT MatrNr FROM Studenten
INNER JOIN Pruefung ON Pruefung.MartNr=Studenten.MatrNr 
GROUP BY Studenten.MatrNr HAVING COUNT(*) > 1);

--22
SELECT MatrNr, Vorname, Nachname, Titel FROM Studenten
INNER JOIN Besuch ON Studenten.MatrNr=Besuch.MartNR INNER JOIN Vorlesungen ON Vorlesungen.VorlNr=Besuch.VorlNr WHERE Vorlesungen.Titel LIKE '%chemie%';


SELECT ' ' AS 'Nothing here';

--DROP VIEW v_Pruefungsplan;

GO
CREATE VIEW v_Pruefungsplan AS
SELECT MatrNr, (Studenten.Vorname + ' ' + Studenten.Nachname) AS Student, NULL AS Wohnort, Pruefungstermin, (Professoren.Vorname + ' ' + Professoren.Nachname) AS Professor, Raum FROM UNIDB.dbo.Studenten
INNER JOIN UNIDB.dbo.Pruefung ON Pruefung.MartNr=Studenten.MatrNr
INNER JOIN UNIDB.dbo.Professoren ON Professoren.PersNr=Pruefung.ProfessorenNr
WHERE Pruefungstermin IS NOT NULL
GO

SELECT * FROM v_Pruefungsplan
GO












